# Creating Cron Jobs
## You may code it in any language but just build it into a container(docker) before shipping into k8s

## Write the script/app for the purpose of the cronjob

## Build docker File to containerized the app
- https://runnable.com/docker/python/dockerize-your-python-application
- https://docs.docker.com/language/python/build-images/

## Create Cron.Job Yaml
```bash
#Either Create Default template then edit from
# Dry run is to test and not create in server
$ kubectl create cronjob <jobname> --image="image" --schedule ="*/5 * * *"  --dry-run=client -o yaml > samepleCronJob.yaml
```

### Test local docker image working properly in local
```bash
$ docker build -t simplejob .
$ docker run -it simplejob --rm simplejob:latest
$ docker ps
```
### Test gitlab docker image working properly in local
```bash
$ docker login registry.gitlab.com/mycronjobs
$ docker pull registry.gitlab.com/cheelim1/mycronjobs:latest
$ docker run --name api-services registry.gitlab.com/cheelim1/mycronjobs:latest
$ docker ps -a
$ docker rm ID_or_Name ID_or_Name

Remove docker image
$ docker images -a
$ docker rmi <image name>
```
- Verify if the docker image is running smoothly locally
- Ref: https://chris-vermeulen.com/using-gitlab-registry-with-kubernetes/ 

### Deploying manually using Kustomize Locally/Minikube
```bash
$ minikube status
$ miniikube start

## Apply locally
$ kubectl get all -n <namespace>
$ kubectl apply -f simpleJobCron.yaml -n sandbox
$ kubectl get cronjobs -n sandbox
$ kubectl get pods -n sandbox | grep simplejob
$ kubectl logs <podname> -n sandbox
$ kubectl delete cronjob simplejob -n sandbox
```

### Ref
- https://medium.com/cubemail88/setting-gitlab-ci-cd-for-python-application-b59f1fb70efe
- https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/
- https://www.youtube.com/watch?v=PUhqw0laR3A 

### Minkube issues
```bash
$ minikube logs
 ```
- https://stackoverflow.com/questions/59958274/unable-to-connect-to-the-server-net-http-tls-handshake-timeout
- https://stackoverflow.com/questions/54264421/suddenly-getting-unable-to-connect-to-the-server-net-http-tls-handshake-timeo